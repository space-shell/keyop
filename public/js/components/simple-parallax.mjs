import SimpleParallax from 'https://space-shell.gitlab.io/simpleParallax/simpleParallax.js'

const style =
  `
    <style>
    </style>
  `

const template = ({ src }) =>
  `
    ${style}

    <img src=${src}>
  `

customElements.define('simple-parallax', class extends HTMLElement {
    constructor(...args) {
      super(...args)

      this.$ = Object.assign({
        orientation: 'up',
        scale: 1.25,
        overflow: false,
        delay: 0,
        transition: 'cubic-bezier(0,0,0,1)',
        breakpoint: 768,
        src: ''
      }, this.dataset)

      this.$.shadow = this.attachShadow({ mode: 'closed' })

      this.$.shadow.innerHTML = template(this.$)

      new SimpleParallax(this.$.shadow.querySelector('img'), this.config)
  }

  get config () {
    return {
      orientation: this.$.orientation,
      scale: Number(this.$.scale),
      overflow: JSON.parse(this.$.overflow),
      delay: Number(this.$.delay),
      transition: this.$.transition,
      breakpoint: Number(this.$.breakpoint)
    }
  }

  connectedCallback () {
  }
})
